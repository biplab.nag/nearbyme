package com.orionsoft.nearbyme.nearbyme.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.orionsoft.nearbyme.nearbyme.MainActivity;
import com.orionsoft.nearbyme.nearbyme.MapNearbyAdapter;
import com.orionsoft.nearbyme.nearbyme.MapNearbyService;
import com.orionsoft.nearbyme.nearbyme.R;
import com.orionsoft.nearbyme.nearbyme.response.MapNearbyResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusFragment extends Fragment {

    private RecyclerView listview;
    private List<MapNearbyResponse.Result> nearbyDatalist;
    private MapNearbyAdapter adapter;
    private MapNearbyService mapNearbyService;

    private Context context;

    private Spinner distSP;
    private Button search_btn;
    private String dist ;
    private TextView nodataFoundTV;

    public static final String NEARBY_BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/";


    public BusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_bus, container, false);

        distSP = (Spinner) view.findViewById(R.id.distSP);
        search_btn = (Button) view.findViewById(R.id.search);
        listview = (RecyclerView) view.findViewById(R.id.nearbyList);

        nodataFoundTV = view.findViewById(R.id.nearbyNotFound);

        final ArrayAdapter<String> distAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, RestaturantFragment.ContainGenerator.getDist());

        distSP.setAdapter(distAdapter);


        distSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        dist = String.valueOf(500);
                        break;
                    case 1:
                        dist = String.valueOf(1000);
                        break;
                    case 2:
                        dist = String.valueOf(1500);
                        break;
                    case 3:
                        dist = String.valueOf(2000);
                        break;
                    case 4:
                        dist = String.valueOf(3000);
                        break;
                    case 5:
                        dist = String.valueOf(4000);
                        break;
                    case 6:
                        dist = String.valueOf(5000);
                        break;
                    case 7:
                        dist = String.valueOf(10000);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                dist = String.valueOf(2000);

            }
        });

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
            }
        });

        if(MainActivity.lastLocation != null){
            getData();
        }
        return view;
    }

    private void getData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NEARBY_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mapNearbyService = retrofit.create(MapNearbyService.class);

        Call<MapNearbyResponse> nearbyResponseCall = mapNearbyService.getNearbyPlacesesData(
                String.valueOf(MainActivity.lastLocation.getLatitude()+","+MainActivity.lastLocation.getLongitude()),
                dist,
                "bus_station",
                getString(R.string.nearby_API_Key)
        );

        nearbyResponseCall.enqueue(new Callback<MapNearbyResponse>() {
            @Override
            public void onResponse(Call<MapNearbyResponse> call, Response<MapNearbyResponse> response) {
                if(response.code()==200){

                    MapNearbyResponse nearbyData = response.body();
                    nearbyDatalist = nearbyData.getResults();
                    context = getContext();

                    adapter = new MapNearbyAdapter(context,nearbyDatalist);
                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    listview.setLayoutManager(llm);
                    listview.setAdapter(adapter);

                    if (nearbyDatalist.size() == 0){
                        nodataFoundTV.setVisibility(View.VISIBLE);
                        nodataFoundTV.setText("No bus station found within "+String.valueOf(Float.valueOf(dist)/1000)+"km");
                    }
                    else {
                        nodataFoundTV.setVisibility(View.GONE);
                    }

                }

            }

            @Override
            public void onFailure(Call<MapNearbyResponse> call, Throwable t) {

            }
        });

    }

}
