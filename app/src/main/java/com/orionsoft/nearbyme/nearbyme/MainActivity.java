package com.orionsoft.nearbyme.nearbyme;

import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.jaeger.library.StatusBarUtil;
import com.orionsoft.nearbyme.nearbyme.animation.ZoomOutTransformation;
import com.orionsoft.nearbyme.nearbyme.fragment.AtmFragment;
import com.orionsoft.nearbyme.nearbyme.fragment.BankFragment;
import com.orionsoft.nearbyme.nearbyme.fragment.BusFragment;
import com.orionsoft.nearbyme.nearbyme.fragment.HospitalFragment;
import com.orionsoft.nearbyme.nearbyme.fragment.PoliceFragment;
import com.orionsoft.nearbyme.nearbyme.fragment.RestaturantFragment;

public class MainActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private TabPagerAdapter adapter;

    private FusedLocationProviderClient client;
    public static Location lastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // transparent status bar
      //  StatusBarUtil.setTransparent(MainActivity.this);

        client = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();




        mViewPager = findViewById(R.id.mViewPager);
        mTabLayout = findViewById(R.id.mTabLayout);

        mTabLayout.addTab(mTabLayout.newTab().setText("Restaurant").setIcon(R.drawable.restrurent));
        mTabLayout.addTab(mTabLayout.newTab().setText("Atm booth").setIcon(R.drawable.atm));
        mTabLayout.addTab(mTabLayout.newTab().setText("Bank").setIcon(R.drawable.bank));
        mTabLayout.addTab(mTabLayout.newTab().setText("Hospital").setIcon(R.drawable.hospital));
        mTabLayout.addTab(mTabLayout.newTab().setText("Bus Station").setIcon(R.drawable.bus));
        mTabLayout.addTab(mTabLayout.newTab().setText("Police Station").setIcon(R.drawable.police));

        adapter = new TabPagerAdapter(getSupportFragmentManager(),mTabLayout.getTabCount());
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                mViewPager.setPageTransformer(true, new ZoomOutTransformation());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void getLastLocation() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},21);
            return;
        }

        client.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {

                if (task.isSuccessful() && task != null) {
                    lastLocation = task.getResult();
                  //  Toast.makeText(MainActivity.this, " "+lastLocation.getLatitude(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    public class TabPagerAdapter extends FragmentPagerAdapter {

        private int tabCount;
        public TabPagerAdapter(FragmentManager fm, int tabCount) {
            super(fm);
            this.tabCount= tabCount;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new RestaturantFragment();
                case 1:
                    return new AtmFragment();
                case 2:
                    return new BankFragment();
                case 3:
                    return new HospitalFragment();
                case 4:
                    return new BusFragment();
                case 5:
                    return new PoliceFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return tabCount;
        }
    }
}
