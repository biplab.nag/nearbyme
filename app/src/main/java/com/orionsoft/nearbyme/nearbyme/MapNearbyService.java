package com.orionsoft.nearbyme.nearbyme;

import com.orionsoft.nearbyme.nearbyme.response.MapNearbyResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MapNearbyService {
    @GET("json?")
    Call<MapNearbyResponse> getNearbyPlacesesData(@Query("location")String latlon,
                                                  @Query("radius")String radious,
                                                  @Query("type")String type,
                                                  @Query("key")String appkey);
}
