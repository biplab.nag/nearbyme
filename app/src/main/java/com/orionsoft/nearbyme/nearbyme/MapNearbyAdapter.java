package com.orionsoft.nearbyme.nearbyme;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.orionsoft.nearbyme.nearbyme.response.MapNearbyResponse;

import junit.framework.Test;

import java.util.List;

public class MapNearbyAdapter extends RecyclerView.Adapter<MapNearbyAdapter.nearbyViewHolder> {


    private Context context;
    private List<MapNearbyResponse.Result> nearbyDataList;


    public MapNearbyAdapter(Context context, List<MapNearbyResponse.Result> nearbyDataList) {
        this.context = context;
        this.nearbyDataList = nearbyDataList;
    }

    @NonNull
    @Override
    public nearbyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v= inflater.inflate(R.layout.card_row,parent,false);
        return new nearbyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull nearbyViewHolder holder, int position) {

        holder.nearbyNameTV.setText(nearbyDataList.get(position).getName().toString());
        holder.nearbyAddress.setText(nearbyDataList.get(position).getVicinity().toString());
        holder.nearbyDistTV.setText(String.valueOf(nearbyDataList.get(position).getRating()));
    }

    @Override
    public int getItemCount() {
        return nearbyDataList.size();
    }

    public class nearbyViewHolder extends RecyclerView.ViewHolder{

        TextView nearbyNameTV;
        TextView nearbyDistTV;
        TextView nearbyAddress;

        public nearbyViewHolder(View itemView) {
            super(itemView);
            nearbyNameTV = itemView.findViewById(R.id.placeName);
            nearbyDistTV = itemView.findViewById(R.id.placedist);
            nearbyAddress = itemView.findViewById(R.id.placeAddress);



        }
    }

}
